from django.test import TestCase, Client,LiveServerTestCase
from django.urls import resolve
from .views import *
from .models import Status
from django.utils import timezone

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time

class UnitTest(TestCase):    
    def test_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
        self.assertNotEqual(response.status_code, 404)

    def test_story_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_story_using_landingpage_templates(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'landing page.html')

    def test_haloApaKabar(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'landing page.html')
        self.assertContains(response, 'HALO, APA KABAR?')

    def test_model_add_status(self):
        new_status = Status.objects.create(status='Pusing Semester 3')
        counting_status_object = Status.objects.all().count()
        self.assertEqual(counting_status_object, 1)

    def test_save_url_is_exist(self):
        response = Client().get('/add_status')
        self.assertEqual(response.status_code,301)

    def test_setelah_save_status(self):
        response = Client().post('/add_status/', data={'status' : 'Pusing Semester 3'})
        counting_status_object = Status.objects.all().count()
        self.assertEqual(counting_status_object, 1)
        self.assertEqual(response.status_code, 302)
        new_response = self.client.get('/')
        html_response = new_response.content.decode('utf-8')
        self.assertIn('Pusing Semester 3', html_response)

    def test_forms_length(self):
        form_data = {'status' : 'Pusing Semester 3'}
        form = StatusForm(data=form_data)
        self.assertTrue(form.is_valid())

    def test_forms_overlength(self):
        form_data = {'status' : 'Pusing Semester 3'*500}
        form = StatusForm(data = form_data)
        self.assertFalse(form.is_valid())

    def test_forms_validation_for_status_blank(self):
        form_data = {'status' : ''}
        form = StatusForm(data = form_data)
        self.assertFalse(form.is_valid())


class Story6FuncTest(LiveServerTestCase):
	def setUp(self):
            self.options = webdriver.ChromeOptions()
            self.options.add_argument('headless')
            self.options.add_argument('no-sandbox')
            self.options.add_argument('disable-dev-shm-usage')
            self.options.add_argument('disable-extensions')
            self.selenium = webdriver.Chrome(options=self.options, executable_path='./chromedriver')
            return super().setUp()

	def tearDown(self):
		self.selenium.quit()
		super(Story6FuncTest, self).tearDown()

	def test_landing_page_element_header(self):
		selenium = self.selenium
		selenium.get(self.live_server_url)
		time.sleep(3)

		header = self.selenium.find_element_by_id('header')
		self.assertEqual(header.find_element_by_tag_name('h2').text, 'HALO, APA KABAR?')

	def test_landing_page_status_form(self):
		selenium = self.selenium
		selenium.get(self.live_server_url)
		time.sleep(3)

		status = selenium.find_element_by_name('status')
		submit = selenium.find_element_by_id('submit')

		message = 'Ngetest'
		status.send_keys(message)
		submit.click()
		time.sleep(3)
		self.assertIn(message, selenium.page_source)

	def test_landing_page_status_form_more_than_300_char(self):
		selenium = self.selenium
		selenium.get(self.live_server_url)
		time.sleep(3)

		status = selenium.find_element_by_name('status')
		submit = selenium.find_element_by_id('submit')

		message = 'Ngetest'*500
		status.send_keys(message)
		submit.click()
		time.sleep(3)
		self.assertNotIn(message, selenium.page_source)








# Create your tests here.
