from django.urls import path
from . import views

app_name = 'homepage'

urlpatterns = [
    path('', views.index, name='landing page'),
    path('add_status/', views.add_status, name='add_status')
]